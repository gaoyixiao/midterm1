package edu.baylor.cs.se.hibernate;

import edu.baylor.cs.se.hibernate.model.Contest;
import edu.baylor.cs.se.hibernate.model.Person;
import edu.baylor.cs.se.hibernate.model.Team;
import edu.baylor.cs.se.hibernate.services.SuperRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.persistence.Query;
import javax.validation.ConstraintViolationException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;


@RunWith(SpringRunner.class)
@DataJpaTest
public class ExampleTest {

    @Autowired
    private TestEntityManager entityManager;

    @Test
    //test the difference between the capacity and occupancy.
    public void queryTeamsTest() throws Exception{
        populate();
        String contestName = "subContest";
        Query query = entityManager.getEntityManager()
                .createQuery("SELECT t FROM Team t WHERE t.contest.name = ?0");
        query.setParameter(0, contestName);
        List<Team> teams = query.getResultList();

        for(Team team : teams) {
            System.out.println("coach name: " + team.getCoach().getName());
            for(Person person: team.getContestants()) {
                System.out.println("contestant name: " + person.getName());
            }
        }
        assertThat(teams.size()).isEqualTo(3);
    }

    @Test
    //test the difference between the capacity and occupancy.
    public void groupByAgeTest() throws Exception{
        populate();
        String contestName = "subContest";
        Query query = entityManager.getEntityManager()
                .createQuery("SELECT year(p.birthdate), count(*) FROM Person p group by year(p.birthdate)");

        List<Object> objects = query.getResultList();
        for(Object object : objects) {
            Object[] pair = (Object[])object;
            System.out.println("The birthdate is: " + (Integer)pair[0]);
            System.out.println("The couting is: " + (Long)pair[1]);
        }

        assertThat(objects.size()).isEqualTo(5);
    }

    @Test
    //test the difference between the capacity and occupancy.
    public void demoTest() throws Exception{
        populate();
        String contestName = "subContest";
        Query query = entityManager.getEntityManager()
                .createQuery("SELECT c.teams.size, c.capacity- c.teams.size FROM Contest c WHERE c.name = ?0");
        query.setParameter(0, contestName);
        List<Object> pair = query.getResultList();
        Object[] objects = (Object[]) pair.get(0);

        System.out.println("The teams size in subContest is: " + objects[0]);
        System.out.println("The difference between capacity and occupancy is: " + objects[1]);
        assertThat(objects[0]).isEqualTo(3);
        assertThat(objects[1]).isEqualTo(1);
    }


    public void populate() throws ParseException {
        Person contestant1 = null;
        Person contestant2 = null;
        Person contestant3 = null;
        Person contestant4 = null;
        Person contestant5 = null;
        Person contestant6 = null;
        Person coach = null;
        Person manager = null;
        try{
            contestant1 = createPerson("Joe", "1996-06-01");
            contestant2 = createPerson("Jan", "1997-06-01");
            contestant3 = createPerson("Joey", "1998-06-01");
            contestant4 = createPerson("Tom", "1997-06-01");
            contestant5 = createPerson("Jerry", "1998-06-01");
            contestant6 = createPerson("Jenny", "1998-06-01");
            coach = createPerson("Coach", "1990-06-01");
            manager = createPerson("Manager", "1992-06-01");

        } catch(Exception e){

        }

        Contest contest = new Contest();
        contest.setName("contest");
        contest.setCapacity(4);
        contest.getManagers().add(manager);
        entityManager.persist(contest);

        Contest subContest = new Contest();
        subContest.setName("subContest");
        subContest.setCapacity(4);
        subContest.getManagers().add(manager);
        entityManager.persist(subContest);

        Team team1 = new Team();
        team1.setContest(subContest);
        team1.setCoach(coach);
        team1.getContestants().add(contestant1);
        team1.getContestants().add(contestant2);
        team1.getContestants().add(contestant3);
        entityManager.persist(team1);

        Team team2 = new Team();
        team2.setContest(subContest);
        team2.setCoach(coach);
        team2.getContestants().add(contestant4);
        team2.getContestants().add(contestant5);
        team2.getContestants().add(contestant6);
        entityManager.persist(team2);

        Team team3 = new Team();
        team3.setContest(subContest);
        team3.setCoach(coach);
        team3.getContestants().add(contestant2);
        team3.getContestants().add(contestant4);
        team3.getContestants().add(contestant6);
        entityManager.persist(team3);
    }

    private Person createPerson(String name, String date) throws Exception{
        DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        Person person = new Person();
        person.setName(name);
        person.setBirthdate(dateFormat1.parse(date));
        entityManager.persist(person);
        return person;
    }
}
