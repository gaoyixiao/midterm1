package edu.baylor.cs.se.hibernate.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Person {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private Date birthdate;

    @Column
    private String email;

    @Column
    private String name;

    @Column
    private String univeristy;

    @ManyToMany(mappedBy = "managers"/*, cascade = CascadeType.ALL, fetch = FetchType.LAZY*/)
    //annotation bellow is just for Jackson serialization in controller
    @JsonIdentityInfo(
            generator = ObjectIdGenerators.PropertyGenerator.class,
            property = "id")
    @JsonIdentityReference(alwaysAsId=true)
    private Set<Contest> contests = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "coach")
    //annotation bellow is just for Jackson serialization in controller
    @JsonIdentityInfo(
            generator = ObjectIdGenerators.PropertyGenerator.class,
            property = "id")
    @JsonIdentityReference(alwaysAsId=true)
    private Set<Team> coachedTeams = new HashSet<>();

    @ManyToMany(mappedBy = "contestants"/*, cascade = CascadeType.ALL, fetch = FetchType.LAZY*/)
    //annotation bellow is just for Jackson serialization in controller
    @JsonIdentityInfo(
            generator = ObjectIdGenerators.PropertyGenerator.class,
            property = "id")
    @JsonIdentityReference(alwaysAsId=true)
    private Set<Team> teams;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUniveristy() {
        return univeristy;
    }

    public void setUniveristy(String univeristy) {
        this.univeristy = univeristy;
    }

    public Set<Team> getCoachedTeams() {
        return coachedTeams;
    }

    public void setCoachedTeams(Set<Team> coachedTeams) {
        this.coachedTeams = coachedTeams;
    }

    public Set<Contest> getContests() {
        return contests;
    }

    public void setContest(Set<Contest> contests) {
        this.contests = contests;
    }

    public Set<Team> getTeams() {
        return teams;
    }

    public void setTeams(Set<Team> teams) {
        this.teams = teams;
    }
}
