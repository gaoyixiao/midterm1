package edu.baylor.cs.se.hibernate.services;

import edu.baylor.cs.se.hibernate.model.Contest;
import edu.baylor.cs.se.hibernate.model.Person;
import edu.baylor.cs.se.hibernate.model.Team;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.transaction.Transactional;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Repository
@Transactional
public class SuperRepository{
    @PersistenceContext
    private EntityManager em;

    /**
     * 1. populate with data (contest, subcontest, 3 teams in subcontest,
     * each with 3 contestants and one coach, add contest manager)
     *
     * 2. select all teams in the given contest and print them (JUnit)
     *
     * 3. provide a report stating how many students there are grouped by age (JUnit)
     *
     * 4. calculate the current contest occupancy and compare with the capacity of the contest (JUnit)
     * */

    public void populate() throws ParseException {
        Person contestant1 = null;
        Person contestant2 = null;
        Person contestant3 = null;
        Person contestant4 = null;
        Person contestant5 = null;
        Person contestant6 = null;
        Person coach = null;
        Person manager = null;
        try{
            contestant1 = createPerson("Joe", "1996-06-01");
            contestant2 = createPerson("Jan", "1997-06-01");
            contestant3 = createPerson("Joey", "1998-06-01");
            contestant4 = createPerson("Tom", "1997-06-01");
            contestant5 = createPerson("Jerry", "1998-06-01");
            contestant6 = createPerson("Jenny", "1998-06-01");
            coach = createPerson("Coach", "1990-06-01");
            manager = createPerson("Manager", "1992-06-01");

        } catch(Exception e){

        }

        Contest contest = new Contest();
        contest.setName("contest");
        contest.setCapacity(4);
        contest.getManagers().add(manager);
        em.persist(contest);

        Contest subContest = new Contest();
        subContest.setName("subContest");
        subContest.setCapacity(4);
        subContest.getManagers().add(manager);
        em.persist(subContest);

        Team team1 = new Team();
        team1.setContest(subContest);
        team1.setCoach(coach);
        team1.getContestants().add(contestant1);
        team1.getContestants().add(contestant2);
        team1.getContestants().add(contestant3);
        em.persist(team1);

        Team team2 = new Team();
        team2.setContest(subContest);
        team2.setCoach(coach);
        team2.getContestants().add(contestant4);
        team2.getContestants().add(contestant5);
        team2.getContestants().add(contestant6);
        em.persist(team2);

        Team team3 = new Team();
        team3.setContest(subContest);
        team3.setCoach(coach);
        team3.getContestants().add(contestant2);
        team3.getContestants().add(contestant4);
        team3.getContestants().add(contestant6);
        em.persist(team3);
    }

    public List<Team> getTeamsByContestName(String contestName){
        return em.createNamedQuery("Team.findTeamsByContestName").setParameter("name",contestName).getResultList();
    }

    public List<Object> groupByAge(){
        return em.createQuery("SELECT year(p.birthdate), count(*) FROM Person p group by year(p.birthdate)").getResultList();
    }

    public List<Object> getTeamsNumber(String contestName){

        //Query query = em.createQuery("SELECT count(*) FROM Team t WHERE t.contest.name = ?0");
        Query query = em.createQuery("SELECT c.teams.size, c.capacity- c.teams.size FROM Contest c WHERE c.name = ?0");
        query.setParameter(0, contestName);
        List<Object> list = query.getResultList();

        //int a = em.createNamedQuery("Contest.findTeamsNumContestName").setParameter("name",contestName).getFirstResult();

        return list;
    }

    private Person createPerson(String name, String date) throws Exception{
        DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        Person person = new Person();
        person.setName(name);
        person.setBirthdate(dateFormat1.parse(date));
        em.persist(person);
        return person;
    }



}
