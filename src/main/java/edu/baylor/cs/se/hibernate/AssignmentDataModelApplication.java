package edu.baylor.cs.se.hibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssignmentDataModelApplication {

    public static void main(String[] args) {
        SpringApplication.run(AssignmentDataModelApplication.class, args);
    }

}
