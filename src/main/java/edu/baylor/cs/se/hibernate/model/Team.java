package edu.baylor.cs.se.hibernate.model;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@NamedQueries({
        @NamedQuery(
                name = "Team.findTeamsByContestName",
                query = "SELECT t FROM Team t JOIN t.contest c WHERE c.name = :name"
        )
})
@Entity
public class Team {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;

    @Column
    private Integer rank;

//    @Enumerated(EnumType.STRING)
//    private State state;
    private String state;

    @ManyToMany//(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "PERSON_TEAM",
            joinColumns = { @JoinColumn(name = "TEAM_ID", referencedColumnName = "ID") }, //do not forget referencedColumnName if name is different
            inverseJoinColumns = { @JoinColumn(name = "PERSON_ID", referencedColumnName = "ID") })
    //annotation bellow is just for Jackson serialization in controller
    @JsonIdentityInfo(
            generator = ObjectIdGenerators.PropertyGenerator.class,
            property = "id")
    @JsonIdentityReference(alwaysAsId=true)
    private Set<Person> contestants = new HashSet<>();

    @ManyToOne
    //annotation bellow is just for Jackson serialization in controller
    @JsonIdentityInfo(
            generator = ObjectIdGenerators.PropertyGenerator.class,
            property = "id")
    @JsonIdentityReference(alwaysAsId=true)
    private Person coach;


    @ManyToOne
    //annotation bellow is just for Jackson serialization in controller
    @JsonIdentityInfo(
            generator = ObjectIdGenerators.PropertyGenerator.class,
            property = "id")
    @JsonIdentityReference(alwaysAsId=true)
    private Contest contest;

    public Long getId() {
     return id;
    }

    public void setId(Long id) {
     this.id = id;
    }

    public String getName() {
     return name;
    }

    public void setName(String name) {
     this.name = name;
    }

    public Integer getRank() {
     return rank;
    }

    public void setRank(Integer rank) {
     this.rank = rank;
    }

    public String getState() {
     return state;
    }

    public void setState(String state) {
     this.state = state;
    }

    public Set<Person> getContestants() {
     return contestants;
    }

    public void setContestants(Set<Person> contestants) {
     this.contestants = contestants;
    }

    public Person getCoach() {
     return coach;
    }

    public void setCoach(Person coach) {
     this.coach = coach;
    }

    public Contest getContest() {
     return contest;
    }

    public void setContest(Contest contest) {
     this.contest = contest;
    }
}
