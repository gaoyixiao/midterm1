package edu.baylor.cs.se.hibernate.controller;

import edu.baylor.cs.se.hibernate.model.Team;
import edu.baylor.cs.se.hibernate.services.SuperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;

@RestController
public class MyController {
    @Autowired
    private SuperRepository superRepository;

    //public MyController(SuperRepository superRepository){
      //  this.superRepository = superRepository;
    //}

    //very bad practise - using GET method to insert something to DB
    @RequestMapping(value = "/populate", method = RequestMethod.GET)
    public ResponseEntity populate() throws ParseException {
        superRepository.populate();
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/queryTeams", method = RequestMethod.GET)
    public ResponseEntity<Team> getTeamsByContestName(@RequestParam(value = "ContestName") String contestName){
        return new ResponseEntity(superRepository.getTeamsByContestName(contestName),HttpStatus.OK);
    }

    @RequestMapping(value = "/queryTeamsNum", method = RequestMethod.GET)
    public ResponseEntity<Team> getTeamsNumByContestName(@RequestParam(value = "ContestName") String contestName){
        return new ResponseEntity(superRepository.getTeamsNumber(contestName),HttpStatus.OK);
    }

    @RequestMapping(value = "/groupByAge", method = RequestMethod.GET)
    public ResponseEntity<Team> groupByAge(){
        return new ResponseEntity(superRepository.groupByAge(),HttpStatus.OK);
    }

//    @RequestMapping(value = "/courses2", method = RequestMethod.GET)
//    public ResponseEntity<Course> getCoursesByStudentName(@RequestParam(value = "name", defaultValue = "Jimmy") String studentName){
//        return new ResponseEntity(superRepository.getCoursesByStudentName(studentName),HttpStatus.OK);
//    }
//
//    @RequestMapping(value = "/queryLocation", method = RequestMethod.GET)
//    public ResponseEntity<Room> getLocationByCourseName(@RequestParam(value = "courseName", defaultValue = "Boring class") String courseName){
//        return new ResponseEntity(superRepository.getLocationByCourseName(courseName),HttpStatus.OK);
//    }

}
